package shapes3d;

public class Test3dShapes {
	
	public static void main(String[] args) {
		Sylinder syl = new Sylinder(5,10);
		double area = syl.area();
		System.out.println(area);
		System.out.println(syl);
		
		Cube cube = new Cube(5);
		System.out.println(cube.area());
		
		System.out.println(cube);
		
		
		}

}
