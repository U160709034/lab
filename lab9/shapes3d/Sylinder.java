package shapes3d;
import shapes2d.Circle;

public class Sylinder extends Circle {
	int height;
	
	public Sylinder(int radius, int height) {
		super(radius);
		this.height = height;
		
	}
	public double area() {
		return 2 * super.area() + 2 * Math.PI * radius * height;
		

	}

		
	
	
	
	
	public String toString() {
		return ("Cylinder [height =" + height +"." + super.toString() + "]");
	}
	public double volume() {
		return super.area()*height;
	}
}
