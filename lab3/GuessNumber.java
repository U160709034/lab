

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
        int numguess=0;
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt(); //Read the user input
         
        while ( number != guess && guess !=-1){
            System.out.println("Sorry!");
            System.out.println("Type -1 to quit or guess another:");
            
            if(number >guess){
                System.out.println("Mine ise greater than your guess.");
                    }
            if(number <guess){
                System.out.println("Mine ise less than your guess.");
                    }
            numguess = numguess +1;
            guess=reader.nextInt();
             }




        if (number==guess){
            System.out.println("Congratulations! You won after" +numguess+" attempts!");
            }
        
        else{
            System.out.println("Sorry,The number was "+number);
        
            }
       

		reader.close(); //Close the resource before exiting
	}
	
	
}
