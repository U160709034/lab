public class GDCRec {

    public static void main(String[] args){
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        int gcd = findGCD(a, b);
        System.out.println("GCD of  " + a + " and " + b + " is " + gcd );

    }
    private static int findGCD(int a, int b){
    	
        if (b ==0)
            return a;
        return findGCD(b,a%b);
    }
}