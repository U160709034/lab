
public class FindGrade {

	public static void main(String[] args){
		int score=Integer.parseInt(args[0]);
	
	if (score<=100 && score>89)
		System.out.println("A");
	else if (score<=89 && score>79)
		System.out.println("B");
	else if (score<=79 && score>69)
		System.out.println("C");
	else if (score<=69 && score>59)
		System.out.println("D");
	else if ( score>=0 && score<59 ) 
		System.out.println("F");
	else 	
		System.out.println("It is not possible.");

	}
}
